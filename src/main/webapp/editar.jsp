<%-- 
    Document   : editar
    Created on : 06-10-2021, 15:48:45
    Author     : axeld
--%>

<%@page import="root.solicitudes.entity.Alumnos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Alumnos alumnos = (Alumnos) request.getAttribute("alumnos");

%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

    </head>
    <body>


        <h1>Editar Solicitud</h1>      

        <form name="form" action="AlumnosController" method="POST">

            <table class="table">
                <thead>
                    <tr>
                        <th>Rut: </th>
                        <th>Nombres: </th>
                        <th>Apellidos: </th>
                        <th>Edad: </th>
                        <th>Establecimiento Eduacional: </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input name="rut" class="form-control" id="rut" 
                                   title="Campo obligatorio" value="<%= alumnos.getRut()%>"></td>
                        <td> <input name="nombres" class="form-control" id="nombres"
                                    value="<%= alumnos.getNombres()%>"></td>
                        <td><input  name="apellidos" class="form-control" id="apellidos"
                                    value="<%= alumnos.getApellidos()%>"          ></td>
                        <td><input name="edad" class="form-control" id="edad"
                                   value="<%= alumnos.getEdad()%>"></td>
                        <td><input  name="establecimiento" class="form-control" id="establecimiento"
                                    value="<%= alumnos.getEstablecimiento()%>"></td>
                    </tr> 
            </table>

            <button type="submit" name="accion" value="grabarEditar" class="btn btn-default" >
                Grabar Cambios
            </button>

        </form>


    </body>                   
</html>
