<%-- 
    Document   : editar
    Created on : 06-10-2021, 15:48:45
    Author     : axeld
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

    </head>
    <body>



        <h1>Ingreso de solicitud de Pase Escolar</h1>      

        <form  name="form" action="AlumnosController" method="POST">

            <table class="table">
                <thead>
                    <tr>
                        <th>Rut: </th>
                        <th>Nombres: </th>
                        <th>Apellidos: </th>
                        <th>Edad: </th>
                        <th>Establecimiento Educacional: </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input name="rut" class="form-control" id="rut" placeholder="Rut" required 
                                   title="Campo obligatorio"></td>
                        <td> <input name="nombres" class="form-control" id="nombres" 
                                    required placeholder="Nombres"></td>
                        <td><input  name="apellidos" class="form-control" id="apellidos" 
                                    required placeholder="Apellidos"></td>
                        <td><input name="edad" class="form-control" id="edad" 
                                   required placeholder="Edad"></td>
                        <td><input  name="establecimiento" class="form-control" id="establecimiento"
                                    required placeholder="establecimiento"></td>
                    </tr> 
                </tbody>
            </table>
            <button type="submit" name="accion" value="grabar" 
                    class="btn btn-default" >Ingresar Solicitud</button>
            <button type="submit"  name="accion" value="listar" formnovalidate="formnovalidate" 
                    class="btn btn-default" >Lista de Solicitudes</button



        </form> 

    </body>
</html>
