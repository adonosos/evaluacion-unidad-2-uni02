<%-- 
    Document   : editar
    Created on : 06-10-2021, 15:48:45
    Author     : axeld
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.solicitudes.entity.Alumnos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Alumnos> lista = (List<Alumnos>) request.getAttribute("lista");

    Iterator<Alumnos> itlista = lista.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

    </head>

    <body>


        <h1>Lista de Alumnos</h1>      




        <form  name="form" action="AlumnosController" method="GET">

            <table class="table" width="50%" border="1" >
                <thead>
                    <tr>
                        <th>Rut </th>
                        <th>Nombres  </th>
                        <th>Apellidos  </th>
                        <th>Edad  </th>
                        <th>Establecimiento Educacional </th>
                    </tr>
                </thead>
                <tbody>
                    <%while (itlista.hasNext()) {
                    Alumnos alumno = itlista.next();%>

                    <tr>
                        <td><%= alumno.getRut()%></td> 
                        <td><%= alumno.getNombres()%></td> 
                        <td><%= alumno.getApellidos()%></td> 
                        <td><%= alumno.getEdad()%></td> 
                        <td><%= alumno.getEstablecimiento()%></td> 
                        <td> <input type="radio" name="seleccion" required value="<%= alumno.getRut()%>"> </td>
                    </tr>
                    <%}%>  


                </tbody>


            </table>

            <button type="submit" name="accion" value="crear" formnovalidate="formnovalidate"  class="btn btn-default" 
                    >Nueva Solicitud</button>
            <button type="submit" name="accion" value="ver" class="btn btn-default" >Editar Solicitud</button>
            <button type="submit" name="accion" value="eliminar" class="btn btn-default" >Eliminar Solicitud</button> 

        </form>



    </body>
</html>
