package root.solicitudes;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures JAX-RS for the application.
 * @author axeld
 */
@ApplicationPath("resources")
public class JAXRSConfiguration extends Application {
    
}
